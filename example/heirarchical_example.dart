// Copyright (c) 2014, The Shelf Route project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

import 'package:shelf/shelf.dart' as shelf;
import 'package:shelf/shelf_io.dart' as io;
import 'package:shelf_route/shelf_route.dart';

void main() {

  var rootRouter = router();

  rootRouter.add('/public', null, _echoRequest, exactMatch: false);

  _addBankingRoutes(rootRouter.child('/banking'));

  var handler = const shelf.Pipeline()
      .addMiddleware(shelf.logRequests())
      .addHandler(rootRouter.handler);

  printRoutes(rootRouter);

  io.serve(handler, 'localhost', 8080).then((server) {
    print('Serving at http://${server.address.host}:${server.port}');
  });
}

shelf.Response _echoRequest(shelf.Request request) {
  return new shelf.Response.ok('Request for "${request.url}"');
}

// supports modularity with routes. i.e. here the banking section has it's own
// routes
// these are relative to where the main router places them
void _addBankingRoutes(Router bankingRouter) {
  var transfersRoute = const shelf.Pipeline()
      .addHandler(_echoRequest);

  bankingRouter.get('/transfer', _echoRequest);

  _addAccountsRoutes(bankingRouter.child('/account'
                                         /*, middleware: authenticator */));

}

void _addAccountsRoutes(Router accountsRouter) {
  _addIndividualAccountRoute(accountsRouter.child('/{accountNumber}'));
  accountsRouter.get('/', _searchAccounts);
}

// a single account with an account number. Note accountNumber is now
// in request.extraParams
void _addIndividualAccountRoute(Router accountRouter) {
  accountRouter
      ..get('/', _fetchAccount)
      ..post('/deposit', _deposit);
}

shelf.Response _fetchAccount(shelf.Request request) {
  return new shelf.Response.ok(
      "Fetch account for acct num ${getPathParameter(request, 'accountNumber')}");
}

shelf.Response _deposit(shelf.Request request) {
  return new shelf.Response.ok(
      "Deposit into account for acct num "
      "${getPathParameter(request, 'accountNumber')}");
}

shelf.Response _searchAccounts(shelf.Request request) {
  return new shelf.Response.ok("Search accounts");
}
