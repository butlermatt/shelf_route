// Copyright (c) 2014, The Shelf Route project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_route.test;

import 'package:shelf/shelf.dart';
import 'package:shelf_route/shelf_route.dart' as r;

import 'package:unittest/unittest.dart';
import 'package:mock/mock.dart';
import 'package:uri/uri.dart';
import 'src/matchers.dart';

Request _request([String pathInfo = "/", String scriptName = "",
                 String method = "GET"]) {
  return new Request(method, Uri.parse('http://localhost/'),
      protocolVersion: "1.1",
      headers: {}, url: Uri.parse(pathInfo), scriptName: scriptName);
}

//typedef Handler(Request request);
abstract class HandlerClass {
  dynamic call(Request request) { return null; }
}

class MockHandler extends Mock implements HandlerClass {
  noSuchMethod(Invocation invocation) => super.noSuchMethod(invocation);
}

//typedef Handler Middleware(Handler innerHandler);
abstract class MiddlewareClass {
  Handler call(Handler innerHandlert) { return null; }
}

class MockMiddleware extends Mock implements MiddlewareClass {
  noSuchMethod(Invocation invocation) => super.noSuchMethod(invocation);
}


void main() {
  MockHandler handler;
  MockHandler middlewareHandler;
  MockMiddleware middleware;
  r.Router router;

  setUp(() {
    handler = new MockHandler();
    router = r.router();
    middleware = new MockMiddleware();
    middlewareHandler = new MockHandler();

    middleware.when(callsTo('call')).alwaysReturn(middlewareHandler);

    middlewareHandler.when(callsTo('call'))
      .alwaysCall((request) => handler.call(request));
  });


  Handler _routeHandler(dynamic path, bool exactMatch) {
    router.add(path, null, handler, exactMatch: exactMatch);
    return router.handler;
  }

  Response _routeRequest(dynamic routePath, String requestPath,
                         {bool exactMatch: false}) =>
      _routeHandler(routePath, exactMatch)(_request(requestPath));



  group('when no routes are defined', () {
    test('an immediate response is returned', () {
      expect(r.router().handler(_request()), new isInstanceOf<Response>());
    });

    test('status code is 404', () {
      expect((r.router().handler(_request()) as Response).statusCode, equals(404));
    });
  });

  group('when one route is defined', () {
    test('then a null path and method matches all', () {
      _routeRequest(null, '/');
      handler.calls('call').verify(happenedOnce);
    });

    test('then a / path and null method matches all', () {
      _routeRequest('/', '/');
      handler.calls('call').verify(happenedOnce);
    });

    test('then an empty path and null method matches all', () {
      _routeRequest('', '/');
      handler.calls('call').verify(happenedOnce);
    });

    group('then when path same as pathInfo', () {
      final String path = '/foo/bar';

      test('route is matched', () {
        _routeRequest(path, path);
        handler.calls('call').verify(happenedOnce);
      });

      test('new pathInfo is empty', () {
        _routeRequest(path, path);
        handler.calls('call', requestWithUrlPath(isEmpty)).verify(happenedOnce);
      });

      test('new scriptName is old pathInfo', () {
        _routeRequest(path, path);
        handler.calls('call', requestWithScriptName(equals(path)))
          .verify(happenedOnce);
      });
    });


    group('then when route path is parent of request pathInfo', () {
      final String routePath = '/foo/bar';
      final String subPath = '/fum/blah';
      final String requestPath = '$routePath$subPath';

      group('and exactMatch is false', () {
        test('route is matched', () {
          _routeRequest(routePath, requestPath);
          handler.calls('call').verify(happenedOnce);
        });

        test('new pathInfo is the sub path of the route', () {
          _routeRequest(routePath, requestPath);
          handler.calls('call', requestWithUrlPath(equals(subPath))).verify(happenedOnce);
        });

        test('new scriptName is old route path', () {
          _routeRequest(routePath, requestPath);
          handler.calls('call', requestWithScriptName(equals(routePath)))
            .verify(happenedOnce);
        });
      });

      group('and exactMatch is true', () {
        test('route is not matched', () {
          _routeRequest(routePath, requestPath, exactMatch: true);
          handler.calls('call').verify(neverHappened);
        });

      });
    });

    group('then when route path is not parent of request pathInfo', () {
      final String routePath = '/foo/bar';
      final String requestPath = '/fool/bar/blah';

      test('route is matched', () {
        _routeRequest(routePath, requestPath);
        handler.calls('call').verify(neverHappened);
      });

      test('status code is 404', () {
        expect(_routeRequest(routePath, requestPath).statusCode, equals(404));
      });
    });


    group('then when route path is not a String', () {
      final String path = '/foo/bar';

      test('then router rejects unsupported path type', () {
        expect(() => router.add(42, null, handler), throwsArgumentError);
      });

      test('then router accepts a UriTemplate and matches', () {
        _routeRequest(r.uriTemplatePattern(path), path);
        handler.calls('call').verify(happenedOnce);
      });

      test('then router accepts a custom pattern and matches', () {
        _routeRequest(new _TestPattern(), path);
        handler.calls('call').verify(happenedOnce);
      });


    });


  });

  group('when routes are nested', () {
    r.Router parentRouter;
    const String parentPath = '/firstseg';

    setUp(() {
      parentRouter = r.router();
      router = parentRouter.child(parentPath);
    });

    Handler _routeHandler(String path) {
      router.add(path, null, handler);
      return parentRouter.handler;
    }

    Response _routeRequest(String routePath, String requestPath) =>
        _routeHandler(routePath)(_request(requestPath));

    group('then when combined paths are same as pathInfo', () {
      final String routePath = '/bar';
      final String requestPath = '$parentPath$routePath';

      test('route is matched', () {
        _routeRequest(routePath, requestPath);
        handler.calls('call').verify(happenedOnce);
      });

      test('new pathInfo is empty', () {
        _routeRequest(routePath, requestPath);
        handler.calls('call', requestWithUrlPath(isEmpty)).verify(happenedOnce);
      });

      test('new scriptName is old pathInfo', () {
        _routeRequest(routePath, requestPath);
        handler.calls('call', requestWithScriptName(equals(requestPath)))
          .verify(happenedOnce);
      });
    });

    group('then when final route is /', () {
      final String routePath = '/';
      final String requestPath = '$parentPath$routePath';

      test('route is matched', () {
        _routeRequest(routePath, requestPath);
        handler.calls('call').verify(happenedOnce);
      });

      test('new pathInfo is empty', () {
        _routeRequest(routePath, requestPath);
        handler.calls('call', requestWithUrlPath(isEmpty)).verify(happenedOnce);
      });

      test('new scriptName is old pathInfo excluding trailing /', () {
        _routeRequest(routePath, requestPath);
        handler.calls('call', requestWithScriptName(equals(parentPath)))
          .verify(happenedOnce);
      });
    });
  });

  group('when path contains a pathVariable', () {

    group('of type String', () {
      final String routePath = '/person/{name}';
      final String requestPath = '/person/fred';

      test('the pathVariable acts like a wildcard for a path segment', () {
        _routeRequest(routePath, requestPath);
        handler.calls('call', requestWithUrlPath(isEmpty))
          .verify(happenedOnce);
        handler.calls('call', requestWithScriptName(equals(requestPath)))
          .verify(happenedOnce);
      });

      test('the value of the pathVariable is available via getPathParameters', () {
        _routeRequest(routePath, requestPath);
        handler.calls('call',
            new HasPathVariables(containsPair('name', 'fred')))
              .verify(happenedOnce);
      });

    });

    group('as the only segment', () {
      final String routePath = '/{name}';
      final String requestPath = '/fred';

      test('the pathVariable acts like a wildcard for a path segment', () {
        _routeRequest(routePath, requestPath);
        handler.calls('call', requestWithUrlPath(isEmpty))
          .verify(happenedOnce);
        handler.calls('call', requestWithScriptName(equals(requestPath)))
          .verify(happenedOnce);
      });

      test('the value of the pathVariable is available via getPathParameters', () {
        _routeRequest(routePath, requestPath);
        handler.calls('call',
            new HasPathVariables(containsPair('name', 'fred')))
              .verify(happenedOnce);
      });

    });

    group('as a query param', () {
      final String routePath = '{?name}';
      final String requestPath = '/?name=fred';

      test('the pathVariable acts like a wildcard for a path segment', () {
        _routeRequest(routePath, requestPath);
        handler.calls('call', requestWithUrlPath(equals('/')))
          .verify(happenedOnce);
        handler.calls('call', requestWithScriptName(isEmpty))
          .verify(happenedOnce);
      });

      test('the value of the pathVariable is available via getPathParameters', () {
        _routeRequest(routePath, requestPath);
        handler.calls('call',
            new HasPathVariables(containsPair('name', 'fred')))
              .verify(happenedOnce);
      });

    });
  });

  group('single http method routes use exact matching', () {
    final String partialPath = '/foo/bar';
    final String fullPath = '$partialPath/fum';

    test('and dont match a longer path on GET', () {
      router.get(partialPath, handler);
      router.handler(_request(fullPath));
      handler.calls('call').verify(neverHappened);
    });
    test('and match an exact path on GET', () {
      router.get(partialPath, handler);
      router.handler(_request(partialPath));
      handler.calls('call').verify(happenedOnce);
    });

    test('and dont match a longer path on PUT', () {
      router.put(partialPath, handler);
      router.handler(_request(fullPath));
      handler.calls('call').verify(neverHappened);
    });
    test('and match an exact path on PUT', () {
      router.put(partialPath, handler);
      router.handler(_request(partialPath, '', 'PUT'));
      handler.calls('call').verify(happenedOnce);
    });

    test('and dont match a longer path on POST', () {
      router.post(partialPath, handler);
      router.handler(_request(fullPath));
      handler.calls('call').verify(neverHappened);
    });
    test('and match an exact path on POST', () {
      router.post(partialPath, handler);
      router.handler(_request(partialPath, '', 'POST'));
      handler.calls('call').verify(happenedOnce);
    });

    test('and dont match a longer path on DELETE', () {
      router.delete(partialPath, handler);
      router.handler(_request(fullPath));
      handler.calls('call').verify(neverHappened);
    });
    test('and match an exact path on DELETE', () {
      router.delete(partialPath, handler);
      router.handler(_request(partialPath, '', 'DELETE'));
      handler.calls('call').verify(happenedOnce);
    });
  });

  group('single http method routes support middleware', () {
    final String partialPath = '/foo/bar';

    test('and invokes middleware on GET', () {
      router.get(partialPath, handler, middleware: middleware);
      router.handler(_request(partialPath));
      middlewareHandler.calls('call').verify(happenedOnce);
      handler.calls('call').verify(happenedOnce);
    });

    test('and invokes middleware on PUT', () {
      router.put(partialPath, handler, middleware: middleware);
      router.handler(_request(partialPath, '', 'PUT'));
      middlewareHandler.calls('call').verify(happenedOnce);
      handler.calls('call').verify(happenedOnce);
    });

    test('and invokes middleware on POST', () {
      router.post(partialPath, handler, middleware: middleware);
      router.handler(_request(partialPath, '', 'POST'));
      middlewareHandler.calls('call').verify(happenedOnce);
      handler.calls('call').verify(happenedOnce);
    });

    test('and invokes middleware on DELETE', () {
      router.delete(partialPath, handler, middleware: middleware);
      router.handler(_request(partialPath, '', 'DELETE'));
      middlewareHandler.calls('call').verify(happenedOnce);
      handler.calls('call').verify(happenedOnce);
    });
  });

  group('fullPaths', () {
    test('returns an empty list when no routes', () {
      expect(router.fullPaths, isEmpty);
    });

    group('for a single route', () {
      _pathInfo() => router.fullPaths.first;

      setUp(() {
        router.get('/foo', handler);
      });

      test('returns an non empty list', () {
        expect(router.fullPaths, isNot(isEmpty));
      });

      test('returns a list of length one', () {
        expect(router.fullPaths, hasLength(1));
      });

      test('returns a list containing a pathInfo with correct method', () {
        expect(_pathInfo().method, equals('GET'));
      });

      test('returns a list containing a pathInfo with correct path', () {
        expect(_pathInfo().path, equals('/foo'));
      });
    });

    test('for two routes returns a list of size two', () {
      router.get('/foo', handler);
      router.put('/blah', handler);
      expect(router.fullPaths, hasLength(2));
    });

    group('for a single route which is a child router', () {
      _pathInfo() => router.fullPaths.first;

      setUp(() {
        router.child('/foo').post('/{blah}', handler);
      });

      test('returns a non empty list', () {
        expect(router.fullPaths, isNot(isEmpty));
      });

      test('returns a list of length one', () {
        expect(router.fullPaths, hasLength(1));
      });

      test('returns a list containing a pathInfo with correct method', () {
        expect(_pathInfo().method, equals('POST'));
      });

      test('returns a list containing a pathInfo with correct path', () {
        expect(_pathInfo().path, equals('/foo/{blah}'));
      });
    });
  });
}

class HasPathVariables extends CustomMatcher {
  HasPathVariables(Matcher matcher)
    : super("Request with path variables that contain",
          "headers", matcher);

  Map<String, Object> featureValueOf(Request actual) =>
      r.getPathParameters(actual);
}

class _TestPattern extends UriPattern {

  @override
  Uri expand(Map<String, Object> parameters) {}

  @override
  UriMatch match(Uri uri) {
    return new UriMatch(this, uri, {}, uri);
  }
}